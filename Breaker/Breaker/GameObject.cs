﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public class GameObject
    {
        public LoadedTexture2D Texture { get; set; }
        public PhysicalBody Body { get; set; }
        public bool IsDead { get; set; }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawCentered(Texture.Sprite, Body.Position, Texture.Width - Texture.MarginWidth * 2, Texture.Height - Texture.MarginHeight * 2);
        }
    }
}
