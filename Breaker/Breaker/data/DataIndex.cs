﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Breaker
{
    [DataContract]
    public class DataIndex
    {
        [DataMember]
        public IEnumerable<TextureData> Textures { get; set; }
    }
}
