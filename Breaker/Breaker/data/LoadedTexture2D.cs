﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public struct LoadedTexture2D
    {
        public Texture2D Sprite { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int MarginWidth { get; set; }
        public int MarginHeight { get; set; }
    }
}
