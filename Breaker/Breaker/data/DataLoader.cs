﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Breaker
{
    public class DataLoader
    {
        private DataLoader()
        {
        }

        private static DataLoader s_Instance;
        public static DataLoader GetDataLoader()
        {
            if (s_Instance == null)
            {
                s_Instance = new DataLoader();
            }
            return s_Instance;
        }

        public DataIndex LoadIndexFrom(string path)
        {
            using (var idxStream = new FileStream(path, FileMode.Open))
            {
                var ser = new DataContractJsonSerializer(typeof(DataIndex));
                var dataIdx = (DataIndex)ser.ReadObject(idxStream);

                dataIdx.Textures.ForEach(td => Console.WriteLine("margin: ({0}, {1})", td.MarginWidth, td.MarginHeight));

                dataIdx.Textures = dataIdx.Textures.Select(td => new TextureData
                {
                    Path = Path.Combine("textures", td.Path),
                    Name = td.Name,
                    Height = td.Height,
                    Width = td.Width,
                    MarginWidth = td.MarginWidth,
                    MarginHeight = td.MarginHeight,
                });
                dataIdx.Textures.ForEach(td => Console.WriteLine("margin: ({0}, {1})", td.MarginWidth, td.MarginHeight));

                return dataIdx;
            }
        }
    }
}
