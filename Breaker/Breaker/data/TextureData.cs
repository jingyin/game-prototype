﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Breaker
{
    [DataContract]
    public struct TextureData
    {
        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Width { get; set; }
        [DataMember]
        public int Height { get; set; }
        [DataMember]
        public int MarginWidth { get; set; }
        [DataMember]
        public int MarginHeight { get; set; }
    }
}
