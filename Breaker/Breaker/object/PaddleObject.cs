﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public class PaddleObject : GameObject
    {
        public int Speed { get; set; }

        private static readonly Dictionary<Keys, Vector2> mapInputKeyToDir;

        static PaddleObject()
        {
            mapInputKeyToDir = new Dictionary<Keys, Vector2>
            {
                { Keys.Left, -Vector2.UnitX },
                { Keys.Right, Vector2.UnitX },
            };
        }

        public void Update(KeyboardState kbdState)
        {
            var dir = mapInputKeyToDir.Keys.Where(k => kbdState.IsKeyDown(k)).Select(k => mapInputKeyToDir[k]).Aggregate(Vector2.Zero, (a, b) => a + b);
            Body.Velocity = dir * Speed;
        }
    }
}
