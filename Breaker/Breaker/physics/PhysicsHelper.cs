﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public static class PhysicsHelper
    {
        private static Interval<double> InfinityInterval = new Interval<double>
        {
            Start = Double.PositiveInfinity,
            End = Double.PositiveInfinity,
        };

        public static double ComputeCollisionTimeToLeftWall(PhysicalBody body, float xCoord)
        {
            if (body.Velocity.X >= 0)
            {
                return Double.PositiveInfinity;
            }

            var distance = xCoord - (body.Position.X - body.Width / 2);
            var time = distance / body.Velocity.X;

            return time < 0 ? Double.PositiveInfinity : time;
        }

        public static double ComputeCollisionTimeToRightWall(PhysicalBody body, float xCoord)
        {
            if (body.Velocity.X <= 0)
            {
                return Double.PositiveInfinity;
            }

            var distance = xCoord - (body.Width / 2 + body.Position.X);
            var time = distance / body.Velocity.X;

            return time < 0 ? Double.PositiveInfinity : time;
        }

        public static double ComputeCollisionTimeToTopWall(PhysicalBody body, float yCoord)
        {
            if (body.Velocity.Y >= 0)
            {
                return Double.PositiveInfinity;
            }

            var distance = yCoord - (body.Position.Y - body.Height / 2);
            var time = distance / body.Velocity.Y;

            return time < 0 ? Double.PositiveInfinity : time;
        }

        public static double ComputeCollisionTimeToBottomWall(PhysicalBody body, float yCoord)
        {
            if (body.Velocity.Y <= 0)
            {
                return Double.PositiveInfinity;
            }

            var distance = yCoord - (body.Position.Y + body.Height / 2);
            var time = distance / body.Velocity.Y;

            return time < 0 ? Double.PositiveInfinity : time;
        }

        public static double ComputeCollisionTimeForDynamicObjectsWithin(PhysicalBody bodyThis, PhysicalBody bodyThat, double dt)
        {
            return ComputeCollisionTimeForDynamicStaticObjectsWithin(
                new PhysicalBody
                {
                    Position = bodyThis.Position,
                    Velocity = bodyThis.Velocity - bodyThat.Velocity,
                    Width = bodyThis.Width,
                    Height = bodyThis.Height,
                },
                new PhysicalBody
                {
                    Position = bodyThat.Position,
                    Velocity = Vector2.Zero,
                    Width = bodyThat.Width,
                    Height = bodyThat.Height,
                },
                dt);
        }

        public static double ComputeCollisionTimeForDynamicStaticObjectsWithin(PhysicalBody bodyThis, PhysicalBody bodyThat, double dt)
        {
            // Console.WriteLine("this: {0}, that: {1}", bodyThis, bodyThis);
            if (bodyThis.Velocity.LengthSquared().IsCloseTo(0))
            {
                return (IsTouchingInX(bodyThis, bodyThat) || IsTouchingInY(bodyThis, bodyThat)) ? 0 : Double.PositiveInfinity;
            }

            Helper.Require(bodyThat.Velocity.LengthSquared().IsCloseTo(0), "static that object");

            // compute HD on x-axis
            var xHD = ComputeHitDurationInAxis(bodyThis.Position.X, bodyThis.Width, bodyThat.Position.X, bodyThat.Width, bodyThis.Velocity.X, dt);

            // compute HD on y-axis
            var yHD = ComputeHitDurationInAxis(bodyThis.Position.Y, bodyThis.Height, bodyThat.Position.Y, bodyThat.Height, bodyThis.Velocity.Y, dt);

            // Console.WriteLine("xHD: {0}, yHD: {1}", xHD, yHD);

            if (xHD.IsIntersecting(yHD))
            {
                return Math.Max(xHD.Start, yHD.Start);
            }
            else
            {
                return Double.PositiveInfinity;
            }
        }

        private static Interval<double> ComputeHitDurationInAxis(float posDynamic, float spanDynamic, float posStatic, float spanStatic, float velocity, double dt)
        {
            if (velocity.IsCloseTo(0))
            {
                var rd = new Interval<float>
                {
                    Start = posDynamic - spanDynamic / 2,
                    End = posDynamic + spanDynamic / 2,
                };

                var rs = new Interval<float>
                {
                    Start = posStatic - spanStatic / 2,
                    End = posStatic + spanStatic / 2,
                };

                return rd.IsIntersecting(rs) ?
                    new Interval<double>
                    {
                        Start = 0,
                        End = dt,
                    } :
                    new Interval<double>
                    {
                        Start = Double.PositiveInfinity,
                        End = Double.PositiveInfinity,
                    };
            }

            var startDynamic = posDynamic + (velocity > 0 ? - spanDynamic : spanDynamic) / 2;
            var endDynamic = posDynamic + velocity * (float)dt + (velocity > 0 ? spanDynamic: -spanDynamic) / 2;

            var rangeDynamic = new Interval<float>
            {
                Start = Math.Min(startDynamic, endDynamic),
                End = Math.Max(startDynamic, endDynamic),
            };
            var rangeStatic = new Interval<float>
            {
                Start = posStatic - spanStatic / 2,
                End = posStatic + spanStatic / 2,
            };

            if (rangeDynamic.IsIntersecting(rangeStatic))
            {
                var intersection = rangeDynamic.Intersects(rangeStatic);
                Helper.Require(intersection.Start <= intersection.End, String.Format("Expects intersection.Start({0}) <= intersection.End({1})", intersection.Start, intersection.End));

                var centerRange = new Interval<float>
                {
                    Start = intersection.Start - spanDynamic / 2,
                    End = intersection.End + spanDynamic / 2,
                };

                Helper.Require(centerRange.Start <= centerRange.End, String.Format("Expects centerRange.Start({0}) <= centerRange.End({1})", centerRange.Start, centerRange.End));

                var timeRange = new Interval<double>
                {
                    Start = ((velocity > 0 ? centerRange.Start : centerRange.End) - posDynamic) / velocity,
                    End = ((velocity > 0 ? centerRange.End : centerRange.Start) - posDynamic) / velocity,
                };

                var bounds = new Interval<double>
                {
                    Start = 0,
                    End = dt,
                };

                if (bounds.IsIntersecting(timeRange))
                {
                    return bounds.Intersects(timeRange);
                }
                else
                {
                    return InfinityInterval;
                }
            }
            else
            {
                return InfinityInterval;
            }
        }

        public static bool IsTouchingInX(PhysicalBody bodyThis, PhysicalBody bodyThat)
        {
            return Math.Abs((bodyThis.Position - bodyThat.Position).X * 2).IsCloseTo(bodyThis.Width + bodyThat.Width);
        }

        public static bool IsTouchingInY(PhysicalBody bodyThis, PhysicalBody bodyThat)
        {
            return Math.Abs((bodyThis.Position - bodyThat.Position).Y * 2).IsCloseTo(bodyThis.Height + bodyThat.Height);
        }
    }
}
