﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public class PhysicalBody
    {
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public override string ToString()
        {
            return String.Format("Dim: ({0}, {1}) @ {2}, moving @ {3}", Width, Height, Position, Velocity);
        }
    }
}
