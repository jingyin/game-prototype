﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public struct Interval<T> where T : struct, IComparable<T>
    {
        public T Start { get; set; }
        public T End { get; set; }

        public bool IsIntersecting(Interval<T> other)
        {
            return !IsNotIntersecting(other);
        }

        public Interval<T> Intersects(Interval<T> other)
        {
            return new Interval<T>
            {
                Start = Helper.Max(Start, other.Start),
                End = Helper.Min(End, other.End),
            };
        }

        private bool IsNotIntersecting(Interval<T> other)
        {
            return End.CompareTo(other.Start) <= 0 || Start.CompareTo(other.End) >= 0;
        }

        public override string ToString()
        {
            return String.Format("[{0}, {1}]", Start, End);
        }
    }
}
