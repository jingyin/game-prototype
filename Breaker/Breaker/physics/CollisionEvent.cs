﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public class CollisionEvent
    {
        public double TimeToElapseInSeconds { get; set; }
        public Action Trigger { get; set; }
        public bool ShouldStop { get; set; }
    }
}
