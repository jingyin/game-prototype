using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;

namespace Breaker
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Dictionary<string, LoadedTexture2D> mapNameToLoadedTexture;
        GameObjectList gameObjectList;
        PaddleObject paddleObject;
        BallObject ballObject;
        SpriteFont gameEndFont;
        KeyboardState previousKbdState;

        int screenWidth;
        int screenHeight;

        bool exited;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            screenWidth = graphics.PreferredBackBufferWidth;
            screenHeight = graphics.PreferredBackBufferHeight;

            gameEndFont = Content.Load<SpriteFont>(@"fonts\segoeKeycaps");

            var dataLdr = DataLoader.GetDataLoader();
            var dataIdx = dataLdr.LoadIndexFrom(@"Content\data\dataIndex.json");

            mapNameToLoadedTexture = dataIdx.Textures.ToDictionary(td => td.Name,
                td => new LoadedTexture2D
                {
                    Sprite = Content.Load<Texture2D>(td.Path),
                    Width = td.Width,
                    Height = td.Height,
                    MarginWidth = td.MarginWidth,
                    MarginHeight = td.MarginHeight,
                });
            mapNameToLoadedTexture.ForEach(kv => Console.WriteLine("[{0}] -> ({1}, {2})", kv.Key, kv.Value.Width, kv.Value.Height));

            Console.WriteLine("screen res: ({0}, {1})", screenWidth, screenHeight);

            gameObjectList = new GameObjectList(screenWidth, screenHeight);

            var paddleTexture = mapNameToLoadedTexture["Paddle Block"];
            var paddleSpeed = 240;
            paddleObject = new PaddleObject
            {
                Texture = paddleTexture,
                Speed = paddleSpeed,
                Body = new PhysicalBody
                {
                    Position = new Vector2(screenWidth / 2, screenHeight - paddleTexture.Height / 2),
                    Velocity = Vector2.Zero,
                    Width = paddleTexture.Width - paddleTexture.MarginWidth * 2,
                    Height = paddleTexture.Height - paddleTexture.MarginHeight * 2,
                },
            };
            gameObjectList.Add(paddleObject);

            var ballTexture = mapNameToLoadedTexture["Ball"];
            var ballSpeed = 250;
            ballObject = new BallObject
            {
                Texture = ballTexture,
                Speed = ballSpeed,
                Body = new PhysicalBody
                {
                    Position = new Vector2(screenWidth / 2, screenHeight / 2),
                    Velocity = new Vector2(0.0f, 1.0f) * ballSpeed,
                    Width = ballTexture.Width - ballTexture.MarginWidth * 2,
                    Height = ballTexture.Height - ballTexture.MarginHeight * 2,
                },
            };
            gameObjectList.Add(ballObject);

            var redBlockTexture = mapNameToLoadedTexture["Red Block"];
            var cyanBlockTexture = mapNameToLoadedTexture["Cyan Block"];
            var yellowBlockTexture = mapNameToLoadedTexture["Yellow Block"];
            var greenBlockTexture = mapNameToLoadedTexture["Green Block"];
            var magentaBlockTexture =mapNameToLoadedTexture["Magenta Block"];

            var rgBlockTexture = new List<LoadedTexture2D> { redBlockTexture, cyanBlockTexture, yellowBlockTexture, greenBlockTexture, magentaBlockTexture };

            foreach (var io in rgBlockTexture.EnumerateWithIndex())
            {
                var blockTexture = io.Item;
                var rowIdx = io.Index;

                var nBlocks = screenWidth / blockTexture.Width;
                for (var i = 0; i < nBlocks; i++)
                {
                    var position = new Vector2(i * blockTexture.Width + blockTexture.Width / 2, rowIdx * blockTexture.Height + blockTexture.Height / 2);
                    gameObjectList.Add(new BrickObject
                    {
                        Texture = blockTexture,
                        Body =new PhysicalBody
                        {
                            Position = position,
                            Velocity = Vector2.Zero,
                            Width = blockTexture.Width - blockTexture.MarginWidth * 2,
                            Height = blockTexture.Height - blockTexture.MarginHeight * 2,
                        },
                    });
                }
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            var currentKbdState = Keyboard.GetState();
            if (Helper.IsKeyFirstDown(currentKbdState, Keys.Escape, previousKbdState))
            {
                if (exited)
                {
                    this.Exit();
                }
                else
                {
                    exited = true;
                }
            }

            if (!exited)
            {
                paddleObject.Update(Keyboard.GetState());

                if (!gameObjectList.Update(gameTime.ElapsedGameTime))
                {
                    // prolly should ease into quitting
                    exited = true;
                }
            }

            previousKbdState = currentKbdState;
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            gameObjectList.DrawAll(spriteBatch);

            if (exited)
            {
                spriteBatch.Begin();
                var gameEndMsg = "GAME OVER";
                var dim = gameEndFont.MeasureString(gameEndMsg);
                spriteBatch.DrawString(gameEndFont, gameEndMsg, new Vector2(screenWidth / 2 - dim.X / 2, screenHeight / 2 - dim.Y / 2), Color.Red);
                spriteBatch.End();
            }

            base.Draw(gameTime);
        }
    }
}
