﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public static class Helper
    {
        public static IEnumerable<Tuple<T1, T2>> Zip<T1, T2>(IEnumerable<T1> rgT1, IEnumerable<T2> rgT2)
        {
            return rgT1.Zip(rgT2, (t1, t2) => Tuple.Create(t1, t2));
        }

        public static bool IsKeyFirstDown(KeyboardState currentKbdState, Keys key, KeyboardState previousKbdState)
        {
            return currentKbdState.IsKeyDown(key) && (previousKbdState == null || previousKbdState.IsKeyUp(key));
        }

        public static T Max<T>(T t1, T t2) where T : struct, IComparable<T>
        {
            return t1.CompareTo(t2) < 0 ? t2 : t1;
        }

        public static T Min<T>(T t1, T t2) where T : struct, IComparable<T>
        {
            return t1.CompareTo(t2) > 0 ? t2 : t1;
        }

        public static void Require(bool cond, string msg)
        {
            if (!cond)
            {
                Console.WriteLine(msg);
                throw new InvalidOperationException(msg);
            }
        }
    }
}
