﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public static class Extensions
    {
        public static void ForEach<T>(this IEnumerable<T> seq, Action<T> action)
        {
            foreach (var t in seq)
            {
                action(t);
            }
        }

        public static void DrawCentered(this SpriteBatch spriteBatch, Texture2D texture, Vector2 center, int width, int height)
        {
            spriteBatch.Draw(texture, new Rectangle((int)center.X - width / 2, (int)center.Y - height / 2, width, height), Color.White);
        }

        public static IEnumerable<IndexedObject<T>> EnumerateWithIndex<T>(this IEnumerable<T> rgObject)
        {
            return Helper.Zip(rgObject, Enumerable.Range(0, rgObject.Count())).
                Select(t => new IndexedObject<T>
                {
                    Item = t.Item1,
                    Index = t.Item2,
                });
        }

        public static IEnumerable<Tuple<T, T>> EnumeratePairWise<T>(this IEnumerable<T> rgObjects)
        {
            var rgIndexedObjects = rgObjects.EnumerateWithIndex();

            return (from s in rgIndexedObjects
                    from t in rgIndexedObjects
                    where s.Index != t.Index
                    select Tuple.Create(s.Item, t.Item));
        }

        public static bool IsCloseTo(this float fThis, float fThat)
        {
            return Math.Abs(fThis - fThat) < 0.01;
        }
    }
}
