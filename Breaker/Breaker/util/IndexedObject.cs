﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public class IndexedObject<T>
    {
        public T Item { get; set; }
        public int Index { get; set; }
    }
}
