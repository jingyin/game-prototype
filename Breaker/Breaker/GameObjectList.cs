﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Breaker
{
    public class GameObjectList
    {
        private List<GameObject> rgGameObjects;
        private List<BrickObject> rgBrickObjects;
        private PaddleObject paddleObject;
        private BallObject ballObject;

        private int screenWidth;
        private int screenHeight;

        public GameObjectList(int width, int height)
        {
            rgGameObjects = new List<GameObject>();
            rgBrickObjects = new List<BrickObject>();
            screenWidth = width;
            screenHeight = height;
        }

        public void Add(GameObject go)
        {
            rgGameObjects.Add(go);

            if (go.GetType() == typeof(PaddleObject))
            {
                paddleObject = go as PaddleObject;
            }
            else if (go.GetType() == typeof(BallObject))
            {
                ballObject = go as BallObject;
            }
            else if (go.GetType() == typeof(BrickObject))
            {
                rgBrickObjects.Add(go as BrickObject);
            }
        }

        public bool Update(TimeSpan dt)
        {
            if (dt > TimeSpan.Zero)
            {
                var rgImminentCollisions = new List<CollisionEvent>();

                // psuedo collision event
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = dt.TotalSeconds,
                    Trigger = () => { },
                });

                // collision between paddle and wall <-- set velocity to zero, hard set position to be just touching the wall
                var paddleBody = paddleObject.Body;
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeToLeftWall(paddleBody, 0),
                    Trigger = () =>
                        {
                            paddleBody.Velocity = Vector2.Zero;
                            paddleBody.Position = new Vector2(paddleBody.Width / 2, paddleBody.Position.Y);
                        },
                });
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeToRightWall(paddleBody, screenWidth),
                    Trigger = () =>
                        {
                            paddleBody.Velocity = Vector2.Zero;
                            paddleBody.Position = new Vector2(screenWidth - paddleBody.Width / 2, paddleBody.Position.Y);
                        },
                });

                // collision between ball and wall <-- reverse component of velocity normal to wall
                var ballBody = ballObject.Body;
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeToTopWall(ballBody, 0),
                    Trigger = () =>
                        {
                            ballBody.Velocity = new Vector2(ballBody.Velocity.X, -ballBody.Velocity.Y);
                            ballBody.Position = new Vector2(ballBody.Position.X, ballBody.Height / 2);
                        },
                });
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeToLeftWall(ballBody, 0),
                    Trigger = () =>
                        {
                            ballBody.Velocity = new Vector2(-ballBody.Velocity.X, ballBody.Velocity.Y);
                            ballBody.Position = new Vector2(ballBody.Width / 2, ballBody.Position.Y);
                        },
                });
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeToRightWall(ballBody, screenWidth),
                    Trigger = () =>
                        {
                            ballBody.Velocity = new Vector2(-ballBody.Velocity.X, ballBody.Velocity.Y);
                            ballBody.Position = new Vector2(screenWidth - ballBody.Width / 2, ballBody.Position.Y);
                        },
                });

                // collision between ball and floor <-- terminate simulation
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeToBottomWall(ballBody, screenHeight),
                    Trigger = () =>
                        {
                            ballBody.Velocity = Vector2.Zero;
                            ballBody.Position = new Vector2(ballBody.Position.X, screenHeight - ballBody.Height / 2);
                        },
                    ShouldStop = true,
                });

                // collision between ball and paddle
                rgImminentCollisions.Add(new CollisionEvent
                {
                    TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeForDynamicObjectsWithin(ballBody, paddleBody, dt.TotalSeconds),
                    Trigger = () =>
                        {
                            Console.WriteLine("ball: {0}, paddle: {1}", ballBody, paddleBody);
                            if (PhysicsHelper.IsTouchingInY(ballBody, paddleBody))
                            {
                                // ballBody.Position = new Vector2(ballBody.Position.X, paddleBody.Position.Y - paddleBody.Height / 2 - ballBody.Height / 2);
                                if (paddleBody.Velocity == Vector2.Zero)
                                {
                                    ballBody.Velocity = new Vector2(ballBody.Velocity.X, -ballBody.Velocity.Y);
                                    Console.WriteLine("reversing ball vertical velocity");
                                }
                                else
                                {
                                    // TODO: this might need some more tweaking
                                    // depending on the point of contact on the paddle
                                    // divide the paddle into 5 sections
                                    // |--|--|--|--|--|
                                    // if hit in the middle section 2, use an angle of 45<deg> w/ vertical depending on where paddle is moving
                                    // if hit in section 1 and 3, use an angle of 30<deg> w/ vertical depending on where paddle is moving
                                    // if hit in section 0 and 4, use an angle of 15<deg> w/ vertical depending on where paddle is moving
                                    var sectionIdx = (int)(((ballBody.Position - paddleBody.Position).X + paddleBody.Width / 2) / paddleBody.Width * 5);
                                    var rgReboundAngleInDeg = new float[] { 15, 30, 45, 30, 15 };
                                    var angleInRad = MathHelper.ToRadians(rgReboundAngleInDeg[Math.Max(0, Math.Min(sectionIdx, rgReboundAngleInDeg.Length - 1))]);
                                    ballBody.Velocity = ballObject.Speed * new Vector2((ballBody.Velocity.X > 0 ? 1 : -1) * (float)Math.Cos(angleInRad), -(float)Math.Sin(angleInRad));
                                    Console.WriteLine("ball: {0}", ballBody);
                                }
                            }
                            else
                            {
                                ballBody.Velocity = -ballBody.Velocity;
                            }
                        },
                });

                // collision between ball and bricks
                rgBrickObjects.ForEach(brickObject =>
                    {
                        var brickBody = brickObject.Body;
                        rgImminentCollisions.Add(new CollisionEvent
                        {
                            TimeToElapseInSeconds = PhysicsHelper.ComputeCollisionTimeForDynamicObjectsWithin(ballBody, brickBody, dt.TotalSeconds),
                            Trigger = () =>
                                {
                                    brickObject.IsDead = true;
                                    Console.WriteLine("ball: {0}, brick: {1}", ballBody, brickBody);
                                    var xMultiplier = PhysicsHelper.IsTouchingInX(ballBody, brickBody) ? -1 : 1;
                                    var yMultiplier = PhysicsHelper.IsTouchingInY(ballBody, brickBody) ? -1 : 1;
                                    ballBody.Velocity = new Vector2(xMultiplier * ballBody.Velocity.X, yMultiplier * ballBody.Velocity.Y);
                                },
                        });
                    });

                rgImminentCollisions.Sort((ceThis, ceThat) => ceThis.TimeToElapseInSeconds.CompareTo(ceThat.TimeToElapseInSeconds));
                var nextCollision = rgImminentCollisions.First();
                rgImminentCollisions = rgImminentCollisions.TakeWhile(ce => ce.TimeToElapseInSeconds == nextCollision.TimeToElapseInSeconds).ToList();

                rgGameObjects.ForEach(go => go.Body.Position += go.Body.Velocity * (float)nextCollision.TimeToElapseInSeconds);
                rgImminentCollisions.ForEach(ce => ce.Trigger());

                if (rgImminentCollisions.Any(ce => ce.ShouldStop))
                    return false;

                dt -= TimeSpan.FromSeconds(nextCollision.TimeToElapseInSeconds);

                // we'll only mark bricks as dead
                if (rgGameObjects.Any(go => go.IsDead))
                {
                    rgGameObjects = rgGameObjects.Where(go => !go.IsDead).ToList();
                    rgBrickObjects = rgBrickObjects.Where(b => !b.IsDead).ToList();
                }

                return Update(dt);
            }

            return true;
        }

        public void DrawAll(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            rgGameObjects.ForEach(go => go.Draw(spriteBatch));
            spriteBatch.End();
        }
    }
}
